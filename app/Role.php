<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return $this->belongsToMany('App\User','userroles'); //in BelongsToMany we need the connection table name unless we will use php convention
    }
}
