<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','department_id',
    ];

    public function candidates(){ 
        return $this->hasMany('App\Candidate'); // each user has many candidates
    }

    public function departments(){ 
        return $this->belongsTo('App\Department'); // each user is only at one department
    }

    public function roles(){
        return $this->belongsToMany('App\Role','userroles'); //in BelongsToMany we need the connection table name unless we will use php convention
    }

    public function interviews(){ 
        return $this->hasMany('App\Interview'); 
    }

    public function isAdmin(){
        $roles = $this->roles; //Query : takes the current user and finds his roles (Admin or Manager or both or nothing)
        if(!isset($roles)) return(FALSE);
        foreach($roles as $role){
            if($role->name === 'admin') return(TRUE);
        }
        return(FALSE);
    }

    public function isManager(){
        $roles = $this->roles;
        if(!isset($roles)) return(FALSE);
        foreach($roles as $role){
            if($role->name === 'manager') return(TRUE); // '===' check id and type
        }
        return(FALSE);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
