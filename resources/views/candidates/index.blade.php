@extends('layouts.app')

@section('title','Candidate list')

@section('content')
@if(Session::has('notallowed'))
<div class="alert alert-danger">
    {{Session::get('notallowed')}}
</div>
@endif
    <div><a href= "{{url('/candidates/create')}}" class="badge badge-primary">Add new Candidate</a></div>
        <h1>List of Candidates</h1>
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
        @endif
        <table class="table table-striped">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th><!-- free text header -->
            </tr>
            <!-- table data -->
            @foreach($candidates as $candidate)
                <tr>
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(isset($candidate->user_id))
                                {{$candidate->owner->name}} <!-- use Relationship -->
                            @else()
                                Assign Owner
                            @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($users as $user) <!-- show all usres in drop-down list -->
                                <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id, $user->id])}}">{{$user->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </td>
                <td>
                    <div class="dropdown">
                        @if(null != App\Status::next($candidate->status_id))
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(isset($candidate->status_id))
                                {{$candidate->situation->name}}
                            @else
                                Define Status
                            @endif
                        </button>
                        @else
                        {{$candidate->situation->name}}
                        @endif
                        
                        @if(App\Status::next($candidate->status_id) != null)
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach(App\Status::next($candidate->status_id) as $status) <!-- show only permitted status in drop-down list -->
                                <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id, $status->id])}}">{{$status->name}}</a>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
                <td><a href = "{{action('CandidatesController@edit', $candidate->id)}}" class="btn btn-warning">Edit</a></td>
                <td><a href = "{{route('candidate.delete', $candidate->id)}}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
    </table>
@endsection