@extends('layouts.app')

@section('title','Create Interview')

@section('content')
    <h1>Create Interview</h1>
    <form method = "post" action = "{{action('InterviewsController@store')}}"> 
        @csrf <!-- protect from csrf attack -->
        <div class="form-group">
            <label for = "name">Interview summary</label>
            <input type = "text" name = "summary" class="form-control" placeholder = "Enter Summary">
        </div>
        <div class="form-group">
            <label for = "email">Interview Date</label>
            <input type = "text" name = "interview_date" class="form-control" placeholder = "Enter Date">
        </div>
        <div class="form-group {{ $errors->has('candidate_id') ? ' has-error' : '' }}">
            <label for="candidate_id" class="col-md-6 col-form-label text-md-left">Candidate</label>
            <div class="col-md">
                <select class="form-control" name="candidate_id">
                <option value="" disabled selected hidden>Choose Candidate</option>
                @foreach($candidates as $candidate)
                    <option value="{{ $candidate->id }}">{{ $candidate->name }}</option>
                @endforeach
                </select>
            </div>
            @if ($errors->has('candidate_id'))
            <span class="help-block">
                <strong>{{ $errors->first('candidate_id') }}</strong>
            </span>
        @endif
        </div>
        <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
            <label for="user_id" class="col-md-6 col-form-label text-md-left">User</label>
            <div class="col-md">
                <select class="form-control" name="user_id">
                <option value="" disabled selected hidden>{{Auth::user()->name}}</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
                </select>
            </div>
            @if ($errors->has('user_id'))
            <span class="help-block">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
        @endif
        </div>
        <div>
            <input class="btn btn-outline-primary" type = "submit" name = "submit" value = "Create interview">
        </div>
    </form>
@endsection
