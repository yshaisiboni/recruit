@extends('layouts.app')

@section('title','Interview list')

@section('content')
    <div><a href= "{{url('/interviews/create')}}" class="badge badge-primary">Add new Inteview</a></div>
        <h1>List of Interviews</h1>
        <table class="table table-striped">
            <tr>
                <th>id</th><th>Summary</th><th>Candidate Name</th><th>Interviewer Name</th><th>Date</th><th>Created</th><th>Updated</th>
            </tr>
            @foreach($interviews as $interview)
                <tr>
                <td>{{$interview->id}}</td>
                <td>{{$interview->summary}}</td>
                <td>{{$interview->candidates->name}}</td>
                <td>{{$interview->users->name}}</td>
                <td>{{$interview->interview_date}}</td>
                <td>{{$interview->created_at}}</td>
                <td>{{$interview->updated_at}}</td>
                </tr>
            @endforeach
    </table>
@endsection