<?php

use Illuminate\Database\Seeder;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'summary' => Str::random(100),
                'interview_date' => date('Y-m-d H:i'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'summary' => Str::random(100),
                'interview_date' => date('Y-m-d H:i'),
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
